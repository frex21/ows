# About

Simple scripts to automatically retrieve stats about the last game that was played from the offical website https://playoverwatch.com

# Requirements
- MongoDB
- Python3
- Modules within requirements.txt

```   
$ pip3 install -r requirements.txt
```

# Usage
- Start MongoDB on the default port **27017**.
- Use **register_player.py** for adding new profiles:

```
$ python3 register_player.py battletag_1 [battletag_n]
$ python3 register_player.py Dude1#2424 Dude2#2392
```

  - Use **update_history.py** after each game to monitor changes.
  - If the flag '**-m**' is provided you may manually enter the name of the map that was played.

```
$ python3 update_history.py
$ python3 update_history.py -m
```
